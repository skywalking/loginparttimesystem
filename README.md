# 兼任助理暨臨時工出勤系統 - 自動簽到

會自動化是因為線上簽到有點麻煩，常常會忘記簽到，不然就是有簽到卻忘記簽退。

## 進行設定

請於 `setting.py` 中設定系統之帳號與密碼 

    login_params = {
            'j_username': '帳號',
            'j_password': '密碼',
            'submit': 'Login'
    }

## 使用方式

    $ python run.py -p <#project> -m <#minute>

## 例子
    $ python run.py -p 3 -m 60     # 計畫3，登錄60分鐘 #

## 安裝問題

程式使用了 [Beautiful Soup](http://www.crummy.com/software/BeautifulSoup/) 模組進行網頁剖析，可用下列命令進行安裝

	$ pip install -r requirements.txt

# 定時排程

若想要每週執行一次登錄程式，也可以丟給 OS 去排程

    $ crontab -e

    # 編輯
    0 8 * * 1 路徑/python 路徑/run.py -p 3 -m 120

每週一早上八點進行登錄，並簽到2小時

