#!/usr/bin/env python
# -*- coding: utf-8 -*-

login_params = {
  'j_username': '帳號',
  'j_password': '密碼',
  'submit': 'Login'
}

header = [ ('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31') ]
