#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cookielib, optparse, setting, urllib, urllib2, sys
from BeautifulSoup import BeautifulSoup
from datetime import datetime
from time import sleep

interval = 10

reload(sys)
sys.setdefaultencoding('utf-8')

# for debug
def getCookieValue(cj):
  for cookie in cj:
    print cookie

def post(url, opener, post_params = None):
  if post_params is None:
    response = opener.open(url)
  else:
    post_params = urllib.urlencode(post_params)
    response = opener.open(url, post_params)
  return response.read()

def parse_signin(content, project = 1):
  info = ()
  bs = BeautifulSoup(content).findAll('tr')[project]
  v = bs.findAll('td')
  k = bs.find('input', {'name': 'signin'})
  info = (k['value'], v[5].text)
  return info

def parse_signout(content):
  bs = BeautifulSoup(content).find('div', {'id': 'body'})
  if bs.text == '您沒有簽到記錄，無法進行簽退 ....':
    return 0
  else:
    info = ()
    v = BeautifulSoup(content).findAll('td')
    k = BeautifulSoup(content).find('input', {'name': 'signout'})
    info = (k['value'], v[16].text)
    return info

def main():
  # init setting
  login_params = setting.login_params
  header = setting.header

  op = optparse.OptionParser("[用法] => python run.py -p <#project> -m <#minute>")
  op.add_option("-p", dest="project", type="int", help="指定計畫")
  op.add_option("-m", dest="minute", type="int", help="登錄多長")
  (options, args) = op.parse_args()
  if options.project is None:
    print op.usage
    print '[例子] => python run.py -p 3 -m 60'
    exit(0)
  else:
    project = options.project
    if options.minute is None:
      minute = 60
    else:
      minute = options.minute
  print '系統設定: 計畫%s，登錄%s分鐘' %(project, minute)

  # save cookie
  cj = cookielib.LWPCookieJar()
  opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
  opener.addheaders = header

  # 登入系統
  login_url = "http://wallaby.cc.ncu.edu.tw/j_spring_security_check"
  ret = post(login_url, opener, login_params)
  if ret:
    print '登入系統'

  # 進行簽到
  signin_url = "http://140.115.182.62/PartTime/parttime.php/signin"
  content = post(signin_url, opener)
  ret = parse_signin(content, project)
  if ret:
    signin_params = { 'signin': ret[0], 'submit': '送出' }
    post(signin_url, opener, signin_params)
    print '進行簽到: ' + ret[1]
    print '簽到時間: ' + datetime.now().strftime('%Y-%m-%d %H:%M:%S')

  # keep session alive
  if minute <= interval:
    sleep(minute * 60)
  else:
    rounds = minute / interval
    remain = minute % interval
    for i in range(rounds):
      signin_url = "http://140.115.182.62/PartTime/parttime.php/signin"
      content = post(signin_url, opener)
      sleep(interval * 60)   # sleep 10 mins
    sleep(remain * 60)

  # 進行簽退
  signout_url = "http://140.115.182.62/PartTime/parttime.php/signout"
  content = post(signout_url, opener)
  ret = parse_signout(content)
  if ret == 0:
    print '您沒有簽到記錄，無法進行簽退 ....'
  else:
    signout_params = { 'signout': ret[0], 'submit': '送出' }
    post(signout_url, opener, signout_params)
    print '進行簽退: ' + ret[1]
    print '簽退時間: ' + datetime.now().strftime('%Y-%m-%d %H:%M:%S')

  # 登出系統
  loginout_url = 'http://wallaby.cc.ncu.edu.tw/logout'
  if post(loginout_url, opener):
    print '登出系統'

if __name__ == '__main__':
  main()
